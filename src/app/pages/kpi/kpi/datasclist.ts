import {Skills} from './skills';

export const datasclist: Skills[] = [
  { id: 1, name: 'Machine Learning' },
  { id: 2, name: 'Data Mining & Management ' },
  { id: 3, name: 'A/B Testing' },
  { id: 4, name: 'Data Extraction / ETL ' },
  { id: 5, name: 'Data Visualization' },
  { id: 6, name: 'Quantitative Analysis' },
  { id: 7, name: 'Other - Data Science & Analytics' }
];







