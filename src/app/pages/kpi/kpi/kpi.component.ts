import { Component, OnInit } from '@angular/core';
import {FormControl} from '@angular/forms';
import {Observable} from 'rxjs';
import {map, startWith} from 'rxjs/operators';


export interface List {

  name: string;
}

@Component({
  selector: 'ngx-kpi',
  templateUrl: './kpi.component.html',
  styleUrls: ['./kpi.component.scss']
})
export class KpiComponent implements OnInit {
  myControl: FormControl = new FormControl();
  options: string[] = ['HTML','CSS3','ANGULAR','JS','C','C++','JAVA'];
  filteredOptions: Observable<string[]>;
  heroes=[''];
  addHero(newHero: string) {
    if (newHero) {
      this.heroes.push(newHero);
    }
  }
  afuConfig = {
    uploadAPI: {
      url:"https://example-file-upload-api"
    }
  };

  ngOnInit() {
    this.filteredOptions = this.myControl.valueChanges.pipe(
      startWith(''),
      map(value => this._filter(value))
    );
  }

  private _filter(value: string): string[] {
    const filterValue = value.toLowerCase();

    return this.options.filter(option => option.toLowerCase().indexOf(filterValue) === 0);
  }


}
