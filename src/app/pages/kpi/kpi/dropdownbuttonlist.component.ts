import { Component, OnInit  ,ChangeDetectionStrategy} from '@angular/core';
import { skillist } from './skillist' ;
import {Skills } from './skills' ;
import {mobileweblist} from './webmobilelist' ;
import {itnetlist} from './itnetlist' ;
import {datasclist} from './datasclist' ;
import {commlist} from './commlist' ;
import {writinglist} from './writinglist' ;
import {designlist} from './designlist' ;
import {WEBMOBILE} from './webmobile' ;
@Component({
  selector: 'ngx-dropdownbuttonlist',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
     
    <div class="dropdown btn-group" ngbDropdown>
        <p *ngFor="let item of SKILLS"   [class.selected]="item === selectedskill" (click)="onSelect(item)"><button nbButton shape="rectangle">{{item.name}} </button></p>
    </div>
        <hr>
        <div *ngIf="selectedskill" class="totheright">

          <h5>Which types of {{selectedskill.name | uppercase}}  do you do?</h5>
          <h6>Select up to 4 types:</h6>
          <div *ngIf="selectedskill.id ==1" >
            <hr>
            <nb-checkbox  *ngFor="let item of WebMOBILE" >{{item.type}}
              <ngx-checkbokskill></ngx-checkbokskill>
              <!--<h6>In which opportunity do you get this knowledge ?</h6>-->
          </nb-checkbox>
            <!--<div>-->
              <!--<ngb-rating [(rate)]="starRate" max=5   *ngFor="let item of WebMOBILE">-->
                <!--<ng-template let-fill="fill">-->
              <!--<span class="star fill" [class.filled]="fill === 100">-->
                <!--<i class="ion-android-star" *ngIf="fill === 100"></i>-->
                <!--<i class="ion-android-star-outline" *ngIf="fill !== 100"></i>-->
              <!--</span>-->
                <!--</ng-template>-->
              <!--</ngb-rating>-->
              <!--<span class="current-rate">{{ starRate }}</span>-->
            <!--</div>-->
          </div>
          <div *ngIf="selectedskill.id ==3" >
            <nb-checkbox status="success"   *ngFor="let item of DATASC"  >{{item.name}}
              <ngx-checkbokskill></ngx-checkbokskill>
              <!--<h6>In which opportunity do you get this knowledge ?</h6>-->
            </nb-checkbox>
            <h6></h6>
          </div>
          <div *ngIf="selectedskill.id ==2" >
            <nb-checkbox status="success"   *ngFor="let item of ITNET"  >{{item.name}}
              <ngx-checkbokskill></ngx-checkbokskill>
              <h6>In which opportunity do you get this knowledge ?</h6>
            </nb-checkbox>
            <br>
          </div>
          <div *ngIf="selectedskill.id ==4" >
            <nb-checkbox status="success"   *ngFor="let item of DESIGN"  >{{item.name}}
              <ngx-checkbokskill></ngx-checkbokskill>
              <!--<h6>In which opportunity do you get this knowledge ?</h6>-->
            </nb-checkbox>
            <br>
          </div>
          <div *ngIf="selectedskill.id ==5" >
            <nb-checkbox status="success"   *ngFor="let item of WRITING"  >{{item.name}}
              <ngx-checkbokskill></ngx-checkbokskill>
              <!--<h6>In which opportunity do you get this knowledge ?</h6>-->
            </nb-checkbox>
            <br>
          </div>
         </div>

     
  `,
  styles: [ `

    .demo-checkboxes-radio {
      display: flex;
      justify-content: space-between;
    }

    .demo-rating {
      display: flex;
      justify-content: space-between;
      flex-wrap: wrap;
    }

    .star {
      font-size: 1.5rem;
      color: nb-theme(color-fg);
    }

    .filled {
      color: nb-theme(color-fg);
    }

    // TODO: Replace with the card header styles mixin
       .rating-header {
         line-height: 2rem;
         font-size: 1.25rem;
         font-family: nb-theme(font-secondary);
         font-weight: nb-theme(font-weight-bolder);
         color: nb-theme(color-fg-heading);
       }

    .current-rate {
      font-size: 1.5rem;
    @include nb-ltr(padding-left, 1rem);
    @include nb-rtl(padding-right, 1rem);
      color: nb-theme(color-fg-heading);
    }

    .dropdown btn-group {
      margin-bottom: 0.25rem;
      width : 10%;
      align-content: right;
    }
    .totheright{
      align-content: right;
    }

     nb-card {
      overflow: visible;
    }
    nb-card-body {
      overflow: visible;
      width:40%;
    }
    .form-inline > * {
      margin: 0 1rem 1rem 0;

    }
  `]
})
export class DropdownbuttonlistComponent implements OnInit {
  starRate = 2;
  WRITING =writinglist;
  COMM=commlist;
  SKILLS= skillist;
  DESIGN= designlist;
  ITNET= itnetlist;
  DATASC = datasclist;
  WebMOBILE= mobileweblist;
  selectedskill: Skills;
  selectedwebm: WEBMOBILE;
  constructor() { }

  ngOnInit() {
  }
  onSelect(skill: Skills): void {
    this.selectedskill = skill;
  }
  onSelect1(webm: WEBMOBILE): void {
    this.selectedwebm = webm;
  }

}
