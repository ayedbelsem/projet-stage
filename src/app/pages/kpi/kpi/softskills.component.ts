import { Component, OnInit } from '@angular/core';
import {commlist} from './commlist' ;
@Component({
  selector: 'ngx-softskills',
  template: `
    <div  *ngFor="let item of COMM " >
     <h4> {{item.name}}</h4>
    <ngx-progresbar></ngx-progresbar>
    </div>
  `,
  styles: [`
  div{
    margin: auto;
    width: 75%;
  }
  h4{
    position: left;
  }`]
})
export class SoftskillsComponent implements OnInit {
COMM=commlist;
  constructor() { }

  ngOnInit() {
  }

}
