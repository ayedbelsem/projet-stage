import {Skills} from './skills';

export const skillist: Skills[] = [
  { id: 1, name: 'Web & Mobile & Software Dev' },
  { id: 2, name: 'IT & Networking ' },
  { id: 3, name: 'Data science & Analytics' },
  { id: 4, name: 'Design & Creative' },
  { id: 5, name: 'Writing' }
];
