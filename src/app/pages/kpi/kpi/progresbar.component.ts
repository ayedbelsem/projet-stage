import { Component } from '@angular/core';

@Component({
  selector: 'ngx-progresbar',
  template: `
    <nb-card>
      <nb-card-body>
        <div class="container">
          <nb-actions size="medium">
            <nb-action icon="nb-arrow-down" (click)="setValue(value - 25)"></nb-action>
          </nb-actions>
          <nb-progress-bar [value]="value" [status]="status" [displayValue]="true"></nb-progress-bar>
          <nb-actions size="medium">
            <nb-action icon="nb-arrow-up" (click)="setValue(value + 25)"></nb-action>
          </nb-actions>
        </div>
      </nb-card-body>
    </nb-card>
  `,
  styles: [`
    .container {
      display: flex;
      align-items: center;
    }
    nb-progress-bar {
      flex: 0.88;
    }
  `]
})
export class ProgresbarComponent {

  value = 25;

  setValue(newValue) {
    this.value = Math.min(Math.max(newValue, 0), 100)
  }

  get status(){
    if (this.value <= 25) {
      return 'danger';
    } else if (this.value <= 50) {
      return 'warning';
    } else if (this.value <= 75) {
      return 'info';
    } else {
      return 'success';
    }
  }

}
