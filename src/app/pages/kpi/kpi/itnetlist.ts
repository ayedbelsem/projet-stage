import {Skills} from './skills';

export const itnetlist: Skills[] = [
  { id: 1, name: 'Database Administration' },
  { id: 2, name: 'Information Security ' },
  { id: 3, name: 'Network & System Administration' },
  { id: 4, name: 'Other - IT & Networking' }
];

