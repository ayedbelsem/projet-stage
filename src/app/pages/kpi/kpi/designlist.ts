
import {Skills} from './skills';

export const designlist: Skills[] = [
  { id: 1, name: ' Animation'  },
  { id: 2, name: ' Audio Production'}   ,
  { id: 3, name: ' Graphic Design' },
  { id: 4, name: ' Illustration  '   },
  { id: 5, name: ' Logo Design & Branding '  },
  { id: 6, name: ' Photography'},
  { id: 7, name: ' Presentations'},
    { id: 8, name: ' Video Production'},
    { id: 9, name: 'Voice Talent'},
  {
    id: 10, name: 'Other - Design & Creative ' }
  ];















