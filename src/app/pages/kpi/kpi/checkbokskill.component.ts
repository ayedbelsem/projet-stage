import { Component, OnInit } from '@angular/core';
import {writinglist} from "./writinglist";
import {commlist} from "./commlist";
import {skillist} from "./skillist";
import {designlist} from "./designlist";
import {itnetlist} from "./itnetlist";
import {datasclist} from "./datasclist";
import {mobileweblist} from "./webmobilelist";
import {Skills} from "./skills";
import {WEBMOBILE} from "./webmobile";

@Component({
  selector: 'ngx-checkbokskill',
  template: `
    <div >
      <ngb-rating [(rate)]="starRate" max=5 >
      <ng-template let-fill="fill">
              <span class="star fill" [class.filled]="fill === 100">
                <i class="ion-android-star" *ngIf="fill === 100"></i>
                <i class="ion-android-star-outline" *ngIf="fill !== 100"></i>
              </span>
      </ng-template>
      </ngb-rating>
      <span class="current-rate">{{ starRate }}</span>
  
    </div>
    <!--<div>-->
      <!--<nb-checkbox status="success"  *ngFor="let item of WebMOBILE"  >{{item.type}} </nb-checkbox>-->
      <!--<ngb-rating [(rate)]="starRate" max=5 >-->
      <!--<ng-template let-fill="fill">-->
              <!--<span class="star fill" [class.filled]="fill === 100">-->
                <!--<i class="ion-android-star" *ngIf="fill === 100"></i>-->
                <!--<i class="ion-android-star-outline" *ngIf="fill !== 100"></i>-->
              <!--</span>-->
      <!--</ng-template>-->
      <!--</ngb-rating>-->
      <!--<span class="current-rate">{{ starRate }}</span>-->
    <!--</div>-->
  `,
  styles: []
})
export class CheckbokskillComponent implements OnInit {
  starRate=5;
  WRITING =writinglist;
  COMM=commlist;
  SKILLS= skillist;
  DESIGN= designlist;
  ITNET= itnetlist;
  DATASC = datasclist;
  WebMOBILE= mobileweblist;
  selectedskill: Skills;
  selectedwebm: WEBMOBILE;
  constructor() { }
  onSelect(skill: Skills): void {
    this.selectedskill = skill;
  }
  onSelect1(webm: WEBMOBILE): void {
    this.selectedwebm = webm;
  }
  ngOnInit() {
  }

}
