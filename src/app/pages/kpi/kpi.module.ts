import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { KpiComponent } from './kpi/kpi.component';
import { ProgresbarComponent } from './kpi/progresbar.component';
import {
  NbStepperModule,
  NbAccordionModule,
  NbListModule,
  NbActionsModule,
  NbProgressBarModule,
  NbCheckboxModule, NbButtonModule,
} from '@nebular/theme';
import {Ng2SmartTableModule} from 'ng2-smart-table';
import {ThemeModule} from '../../@theme/theme.module';
import {DropdownbuttonlistComponent} from './kpi/dropdownbuttonlist.component';
import {SoftskillsComponent} from './kpi/softskills.component';
import {StatisComponent} from './kpi/statis.component';
import {ChartModule} from 'angular2-chartjs';
import {MatAutocompleteModule} from '@angular/material/autocomplete'
import {FormsModule} from "@angular/forms";
import { AngularFileUploaderModule } from "angular-file-uploader";

import { NgxEchartsModule } from 'ngx-echarts';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { GraphComponent } from './kpi/graph.component';
import { CheckbokskillComponent } from './kpi/checkbokskill.component';
@NgModule({
  imports: [
    ThemeModule,
    CommonModule,
    NbStepperModule,
    NbAccordionModule,
    NbListModule,
    NbActionsModule,
    NbProgressBarModule,
    Ng2SmartTableModule,
    NbCheckboxModule,
    ChartModule,
    MatAutocompleteModule,
    FormsModule,AngularFileUploaderModule, NgxEchartsModule,NgxChartsModule,NbButtonModule
  ],
  exports:[ MatAutocompleteModule],
  declarations: [KpiComponent, ProgresbarComponent, DropdownbuttonlistComponent, SoftskillsComponent, StatisComponent, GraphComponent, CheckbokskillComponent ],
})
export class KpiModule { }
