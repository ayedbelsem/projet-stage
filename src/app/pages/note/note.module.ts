import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NoteComponent } from './note/note.component';
import {ThemeModule} from '../../@theme/theme.module';
import {
  NbAccordionModule,
  NbActionsModule, NbCheckboxModule,
  NbLayoutModule,
  NbListModule, NbProgressBarModule,
  NbSidebarModule,
  NbStepperModule
} from '@nebular/theme';
import { GraphnoteComponent } from './note/graphnote.component';
import {ChartModule} from "angular2-chartjs";
import {MatAutocompleteModule} from "@angular/material/autocomplete";
import {FormsModule} from "@angular/forms";
import {AngularFileUploaderModule} from "angular-file-uploader";
import {NgxEchartsModule} from "ngx-echarts";
import {NgxChartsModule} from "@swimlane/ngx-charts";
import {Ng2SmartTableModule} from "ng2-smart-table";


@NgModule({
  imports: [
    ThemeModule, CommonModule,  NbLayoutModule,
    NbSidebarModule,
    NbStepperModule,
    NbAccordionModule,
    NbListModule,
    NbActionsModule,
    NbProgressBarModule,
    Ng2SmartTableModule,
    NbCheckboxModule,
    ChartModule,
    MatAutocompleteModule,
    FormsModule,AngularFileUploaderModule, NgxEchartsModule,NgxChartsModule
  ],
  declarations: [NoteComponent, GraphnoteComponent]
})
export class NoteModule {
}
