import { Component} from '@angular/core';
import {ExcelService} from '../../service/exel.service';
@Component({
  selector: 'ngx-xlexport',
  template: `
    <button (click)="exportAsXLSX()"><i class="fa fa-file-excel-o" style="font-size:48px;color:blue"></i></button>
    <table>
      <tr>
        <td>Eid</td>
        <td>Ename</td>
        <td>Esal</td>
      </tr>
      <tr *ngFor="let item of data">
        <td>{{item.eid}}</td>
        <td>{{item.ename}}</td>
        <td>{{item.esal}}</td>
      </tr>
    </table>
  `,
  styles: []
})
export class XlexportComponent{
  name = 'Angular 6';
  data: any = [{
    eid: 'e101',
    ename: 'ravi',
    esal: 1000
  },
    {
      eid: 'e102',
      ename: 'ram',
      esal: 2000
    },
    {
      eid: 'e103',
      ename: 'rajesh',
      esal: 3000
    }];
  constructor(private excelService:ExcelService){

  }
  exportAsXLSX():void {
    this.excelService.exportAsExcelFile(this.data, 'sample');
  }
}
