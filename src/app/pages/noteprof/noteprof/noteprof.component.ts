import { Component} from '@angular/core';
import {LocalDataSource} from "ng2-smart-table";
import {SmartTableService} from "../../../@core/data/smart-table.service";


@Component({
  selector: 'ngx-noteprof',
  templateUrl: './noteprof.component.html',
  styleUrls: ['./noteprof.component.scss']
})
export class NoteprofComponent {
  settings = {
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    columns: {
      id: {
        title: 'ID',
        type: 'number',
      },
      firstName: {
        title: 'First Name',
        type: 'string',
      },
      lastName: {
        title: 'Last Name',
        type: 'string',
      },
      email: {
        title: 'E-mail',
        type: 'string',
      },
      NoteCC: {
        title: 'CC',
        type: 'number',
      },
      NoteTp: {
        title: 'Tp',
        type: 'number',
      },
     NoteExamen: {
        title: 'Examen',
        type: 'number',
      },
    },
  };

source: LocalDataSource = new LocalDataSource();

  constructor(private service: SmartTableService) {
    const data = this.service.getData();
    this.source.load(data);
  }
  //
  // exportAsXLSX(service: SmartTableService):void {
  //   const data = this.service.getData();
  //   this.excelService.exportAsExcelFile(data, 'sample');
  // }
  onDeleteConfirm(event): void {
    if (window.confirm('Are you sure you want to delete?')) {
      event.confirm.resolve();
    } else {
      event.confirm.reject();
    }
  }
}
