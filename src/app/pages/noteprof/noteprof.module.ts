import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NoteprofComponent } from './noteprof/noteprof.component';
import {Ng2SmartTableModule} from "ng2-smart-table";
import {ThemeModule} from "../../@theme/theme.module";
import {SmartTableService} from "../../@core/data/smart-table.service";
import { XlexportComponent } from './noteprof/xlexport.component';

@NgModule({
  imports: [
    CommonModule, Ng2SmartTableModule , ThemeModule,
  ],
  declarations: [NoteprofComponent, XlexportComponent],
  providers: [
    SmartTableService,
  ],
})
export class NoteprofModule { }
