import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CalenderComponent } from './calender/calender.component';
import {
  NbStepperModule,
  NbAccordionModule,
  NbListModule,
  NbActionsModule,
  NbProgressBarModule,
  NbCheckboxModule, NbCalendarModule
} from '@nebular/theme';
import {Ng2SmartTableModule} from 'ng2-smart-table';
import {ThemeModule} from '../../@theme/theme.module';
@NgModule({
  imports: [
    CommonModule, NbStepperModule,
    NbAccordionModule,
    NbListModule,
    NbActionsModule,
    NbProgressBarModule,
    NbCheckboxModule,Ng2SmartTableModule
    ,ThemeModule,NbCalendarModule,
  ],
  declarations: [CalenderComponent]
})
export class CalenderModule { }
